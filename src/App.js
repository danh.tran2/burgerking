import React, { Component, Fragment } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// init git with bookticket proj
import { HomeTemplate } from "./bookticket/templates/HomeTemplate/HomeTemplate";
import { AdminTemplate } from "./bookticket/templates/AdminTemplate/AdminTemplate";
import Home from "./bookticket/pages/Home/Home";
import Login from "./bookticket/pages/Login/Login";
import Admin from "./bookticket/pages/Admin/Admin";

// no redux proj
import BaiTapThuKinh from "./noRedux/BaiTapThuKinh/BaiTapThuKinh";
import BaiTapChonXe from "./noRedux/BaiTapChonXe/BaiTapChonXe";
import DanhSachSanPham from "./demo/PropsModal/DanhSachSanPham";
import Modal from "./demo/PropsModal/Modal";

// demo feature
import RenderWithMap from "./demo/RenderWithMap/RenderWithMap";
import StateUpdating from "./demo/StateUpdating/StateUpdating";
import PropDemo from "./demo/PropsDemo/PropDemo";
import DataBinding from "./demo/DataBinding/DataBinding";
import EventHandling from "./demo/EventHandling/EventHandling";
import BaiTapChonXeRedux from "./wRedux/BaiTapChonXeRedux/BaiTapChonXeRedux";
import BaiTapGameBauCua from "./wRedux/BaiTapGameBauCua/BaiTapGameBauCua";

export default class App extends Component {
  state = {
    thongTinSanPham: {
      tenSP: "Tên mặc định",
      hinhAnh: "",
      gia: 1000,
      moTa: "trống",
    },
  };

  xemChiTiet = (sanPhamChiTiet) => {
    // lấy thông tin chi tiết sản phẩm
    this.setState({ thongTinSanPham: sanPhamChiTiet });
  };

  render() {
    let sinhVien = {
      ma: 1,
      ten: "Tèo",
      tuoi: 19,
    };

    let mangSP = [
      {
        maSP: 1,
        tenSP: "black car",
        hinhAnh: "./img/products/black-car.jpg",
        gia: 1000,
      },
      {
        maSP: 2,
        tenSP: "red car",
        hinhAnh: "./img/products/red-car.jpg",
        gia: 2000,
      },
      {
        maSP: 3,
        tenSP: "silver car",
        hinhAnh: "./img/products/silver-car.jpg",
        gia: 2000,
      },
      {
        maSP: 4,
        tenSP: "steel car",
        hinhAnh: "./img/products/steel-car.jpg",
        gia: 2000,
      },
    ];

    return (
      <BrowserRouter>
        <Fragment>
          <Switch>
            {/* init with bookticket proj */}
            <Route exact path="/login" component={Login} />
            <AdminTemplate exact path="/admin" component={Admin} />
            <HomeTemplate exact path="/" component={Home} />

            {/* no redux proj*/}
            <Route exact path="/northukinh" component={BaiTapThuKinh} />
            <Route exact path="/norchonxe" component={BaiTapChonXe} />
            <Route
              exact
              path="/demopropmodal"
              render={(props) => (
                <DanhSachSanPham
                  mangSanPham={mangSP}
                  xemChiTiet={this.xemChiTiet}
                  {...props}
                />
              )}
            />

            {/* with redux */}
            <Route exact path="/redchonxe" component={BaiTapChonXeRedux} />
            <Route exact path="/game" component={BaiTapGameBauCua} />
            {/* 
            nên dùng render chỗ này chứ không phải component,
            https://github.com/ReactTraining/react-router/issues/4105 
            https://tylermcginnis.com/react-router-pass-props-to-components/
            */}

            {/* demo feature */}
            <Route exact path="/demomapren" component={RenderWithMap} />
            <Route exact path="/demodatabind" component={DataBinding} />
            <Route exact path="/demoeventhandle" component={EventHandling} />
            <Route exact path="/demostateup" component={StateUpdating} />
            <Route
              exact
              path="/demoprop"
              component={() => (
                <PropDemo title="FrontEnd 38" ttSinhVien={sinhVien} />
              )}
            />

          </Switch>
          <Modal thongTinSanPham={this.state.thongTinSanPham} />
        </Fragment>
      </BrowserRouter>
    );
  }
}
