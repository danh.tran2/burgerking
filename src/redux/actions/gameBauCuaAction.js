import { PLAY_GAME, DAT_CUOC } from "../types/gameBauCuaType";

// File lưu trữ các object dispatch lên reducer

export const playGameAction = () => {
  return {
    type: PLAY_GAME,
  };
};


export const datCuocAction = (quanCuoc) => {
  return {
    type: DAT_CUOC,
    quanCuoc,
  };
};
