import axios from "axios";
import { LAY_DANH_SACH_PHIM } from "../types/quanLyPhimType";
// ------------------ActionReducer----------
export const layDanhSachPhimAction = () => {

  return (dispatch) =>
    axios({
      url:
        "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01", //link request
      method: "GET", // phương thức backend quy định cho link api
    })
      .then((res) => {
        // Dispatch action tại đây
        dispatch({
          type: LAY_DANH_SACH_PHIM,
          dsPhim: res.data,
        });
        //   console.log("data", res);
        //   this.setState({ danhSachPhim: res.data });
      })
      .catch((err) => {
        console.log("err", err.response.data);
      });
  console.log("componentDidMount");
};

