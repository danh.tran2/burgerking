const stateGioHang = [
  // { maSP: 4523, tenSP: "test1", hinhAnh: "", gia: 2000, soLuong: 1 },
];

const GioHangReducer = (state = stateGioHang, action) => {
  // console.log("gioHangReducer", action);
  switch (action.type) {
    case "THEM_GIO_HANG": {
      // Xử lý nghiệp vụ thêm giỏ hàng
      // B1: duyệt xem giỏ hàng có sản phẩm đó hay không
      const gioHangUpdate = [...state];
      let index = gioHangUpdate.findIndex(
        (spGioHang) => spGioHang.maSP === action.sanPhamGioHang.maSP
      );
      if (index !== -1) {
        //nếu sản phẩm trong giỏ hàng tồn tại thì tăng số lượng và trả về giỏ hàng mới
        gioHangUpdate[index].soLuong += 1;
        return gioHangUpdate;
      }
      //   nếu sản phẩm chưa tồn tại trong giỏ hàng thì lấy giỏ hàng push thêm action.sanPhamGioHang
      //   state.push(action.sanPhamGioHang);
      // return state;
      return [...state, action.sanPhamGioHang];
    }
    case "XOA_GIO_HANG": {
      // Tìm ra phần tử cần xóa trong giỏ hàng
      let index = state.findIndex(
        (spGioHang) => spGioHang.maSP === action.maSP
      );
      if (index !== -1) {
        // xóa phần tử trong giỏ hàng
        state.splice(index, 1);
        return [...state];
        // nếu chỉ 'return state' thì reducer sẽ ko lấy giá trị state đã đổi dù clg vẫn thấy đúng,
        // phải gán vô mảng mới !!! => tính chất bất biến của redux, bởi vậy hay tạo ra biến mới
      }
      return state;
    }
    case "THEM_SO_LUONG": {
      const gioHangUpdate = [...state];
      let index = state.findIndex(
        (spGioHang) => spGioHang.maSP === action.maSP
      );
      if (index !== -1) {
        gioHangUpdate[index].soLuong++;
        return gioHangUpdate;
      }
      return state;
    }
    // case "XOA_SO_LUONG": {
    //   const gioHangUpdate = [...state];
    //   let index = state.findIndex(
    //     (spGioHang) => spGioHang.maSP === action.maSP
    //   );
    //   if (index !== -1) {
    //     if (gioHangUpdate[index].soLuong === 1) {
    //       state.splice(index, 1);
    //       return [...state];
    //     } else {
    //       gioHangUpdate[index].soLuong--;
    //       return gioHangUpdate;
    //     }
    //   }
    //   return state;

    // }
    case "TANG_GIAM_SL": {
      // Tìm sp cần tăng giảm dựa vào mã sp
      const gioHangUpdate = [...state];

      let index = gioHangUpdate.findIndex(
        (spGioHang) => spGioHang.maSP === action.maSP
      );
      if (index !== -1) {
        if (action.tangGiam) {
          gioHangUpdate[index].soLuong++;
        } else {
          if (gioHangUpdate[index].soLuong > 1) {
            gioHangUpdate[index].soLuong--;
          } else {
            alert("Số lượng tối thiểu phải là 1");
          }
        }
      }
      return gioHangUpdate;
    }
    default:
      return state;
  }
  // return state;
};

export default GioHangReducer;
