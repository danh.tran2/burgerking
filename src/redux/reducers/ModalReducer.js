// Model reducer là state của modal - tên khác nhưng cùng là 1

const stateModal = {
    thongTinSanPham: {
        tenSP: 'sxdvsd',
        hinhAnh: '',
        gia: 1000
    }
}


const stateModelReducer = (state=stateModal, action) => {
    // action là dữ liệu được dispatch từ component
    switch (action.type){
        // xử lý setState
        case "XEM_CHI_TIET": {
            state.thongTinSanPham = action.sanPham;
            return {...state};
            // thêm ... để tạo ra 1 biến mới để redux xử lý
        }
        default:
            return {...state}
    }
    // return {...state};
}

export default stateModelReducer;