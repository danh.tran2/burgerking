import { DAT_CUOC, PLAY_GAME } from "../types/gameBauCuaType";

const stateGame = {
  tongTien: 100,
  danhSachCuoc: [
    { ma: "bau", hinhAnh: "./img/gameImg/bau.PNG", diemCuoc: 0 },
    { ma: "cua", hinhAnh: "./img/gameImg/cua.PNG", diemCuoc: 0 },
    { ma: "ca", hinhAnh: "./img/gameImg/ca.PNG", diemCuoc: 0 },
    { ma: "ga", hinhAnh: "./img/gameImg/ga.PNG", diemCuoc: 0 },
    { ma: "nai", hinhAnh: "./img/gameImg/nai.PNG", diemCuoc: 0 },
    { ma: "tom", hinhAnh: "./img/gameImg/tom.PNG", diemCuoc: 0 },
  ],
  xucXac: [
    { ma: "tom", hinhAnh: "./img/gameImg/tom.PNG", diemCuoc: 0 },
    { ma: "tom", hinhAnh: "./img/gameImg/ga.PNG", diemCuoc: 0 },
    { ma: "tom", hinhAnh: "./img/gameImg/nai.PNG", diemCuoc: 0 },
  ],
};

const GameBauCuaReducer = (state = stateGame, action) => {
  switch (action.type) {
    case DAT_CUOC: {
      console.log(action)
      // Tìm vị trí quân cuộc trong danhSachCuoc -> Xử lý tăng điểm
      let danhSachCuocUpdate = [...state.danhSachCuoc];
      let index = danhSachCuocUpdate.findIndex(
        (quanCuoc) => quanCuoc.ma === action.quanCuoc.ma
      );
      if (index !== -1) {
        if (state.tongTien > 0) {
          danhSachCuocUpdate[index].diemCuoc += 10;
          state.tongTien -= 10;
        } else {
          alert("Tổng tiền phải lớn hơn 0");
        }
      }
      return { ...state, danhSachCuoc: danhSachCuocUpdate };
    }
    case PLAY_GAME: {
      let mangXucXacNgauNhien = [];
      for (let i = 0; i < 3; i++) {
        let soNgauNhien = Math.floor(Math.random() * 6);
        let xucXacNgauNhien = {
          ma: state.danhSachCuoc[soNgauNhien].ma,
          hinhAnh: state.danhSachCuoc[soNgauNhien].hinhAnh,
        };
        mangXucXacNgauNhien.push(xucXacNgauNhien);
        // random 3 lần và tạo xúc xắc 3 lần
      }
      mangXucXacNgauNhien.forEach((xucXac, index) => {
        let indexQuanCuoc = state.danhSachCuoc.findIndex(
          (quanCuoc) => quanCuoc.ma === xucXac.ma
        );
        // Cập nhập tổng tiền
        if (indexQuanCuoc !== -1) {
          state.tongTien += state.danhSachCuoc[indexQuanCuoc].diemCuoc;
        }
      });
      // xử lý hoàn tiền
      state.danhSachCuoc.forEach((quanCuoc, index) => {
        let indexXucXac = mangXucXacNgauNhien.findIndex(
          (xucXacNN) => xucXacNN.ma === quanCuoc.ma
        );
        if (indexXucXac !== -1) {
          state.tongTien += quanCuoc.diemCuoc;
        }
      });
      // reset mảng cược
      let resetMangCuoc = state.danhSachCuoc.map((quanCuoc, index) => {
        return { ...quanCuoc, diemCuoc: 0 };
      });

      return {
        ...state,
        xucXac: mangXucXacNgauNhien,
        danhSachCuoc: resetMangCuoc,
      };
    }

    default:
      return {...state}
  }
  // return { ...state };
};

export default GameBauCuaReducer;
